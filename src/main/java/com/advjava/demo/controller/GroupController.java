package com.advjava.demo.controller;

import com.advjava.demo.repository.GroupRepository;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;

public class GroupController {

    private final GroupRepository groupRepository;

    public GroupController(GroupRepository groupRepository) {
        this.groupRepository = groupRepository;
    }
    @GetMapping("/api/groups")
    public ResponseEntity<?> getGroups() {

        return ResponseEntity.ok(groupRepository.findAll());
    }
}
